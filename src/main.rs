extern crate image;
extern crate rayon;

use rayon::prelude::*;

use image::RgbImage;

fn apply_5x5(data: &[u8], w: usize, h: usize, matrix: &[[isize; 5]; 5]) -> Vec<isize> {
    (0..data.len())
        .map(|idx| {
            let mut res = data[idx] as isize * matrix[2][2];
            let x = idx / w;
            let y = idx % w;

            let toptop = x > 2;
            let top = x > 1;
            let leftleft = y > 2;
            let left = y > 1;
            let bottombottom = x < (h - 2);
            let bottom = x < (h - 1);
            let rightright = y < (w - 2);
            let right = y < (w - 1);

            // Top rows
            if top {
                if toptop {
                    if left {
                        if leftleft {
                            res += data[(x - 2) * w + (y - 2)] as isize * matrix[0][0];
                        }
                        res += data[(x - 2) * w + (y - 1)] as isize * matrix[0][1];
                    }
                    res += data[(x - 2) * w + y] as isize * matrix[0][2];
                    if right {
                        res += data[(x - 2) * w + y + 1] as isize * matrix[0][3];
                        if rightright {
                            res += data[(x - 2) * w + y + 2] as isize * matrix[0][4];
                        }
                    }
                }
                res += data[(x - 1) * w + y] as isize * matrix[1][2];
                if left {
                    if leftleft {
                        res += data[(x - 1) * w + (y - 2)] as isize * matrix[1][0];
                    }
                    res += data[(x - 1) * w + (y - 1)] as isize * matrix[1][1];
                    if right {
                        res += data[(x - 1) * w + y + 1] as isize * matrix[1][3];
                        if rightright {
                            res += data[(x - 1) * w + y + 2] as isize * matrix[1][4];
                        }
                    }
                }
            }

            // middle row
            if left {
                if leftleft {
                    res += data[x * w + y - 2] as isize * matrix[2][0];
                }
                res += data[x * w + y - 1] as isize * matrix[2][1];
            }
            if right {
                res += data[x * w + y + 1] as isize * matrix[2][3];
                if rightright {
                    res += data[x * w + y + 2] as isize * matrix[2][4];
                }
            }

            // bottom rows
            if bottom {
                if bottombottom {
                    if left {
                        if leftleft {
                            res += data[(x + 2) * w + (y - 2)] as isize * matrix[4][0];
                        }
                        res += data[(x + 2) * w + (y - 1)] as isize * matrix[4][1];
                    }
                    res += data[(x + 2) * w + y] as isize * matrix[4][2];
                    if right {
                        res += data[(x + 2) * w + y + 1] as isize * matrix[4][3];
                        if rightright {
                            res += data[(x + 2) * w + y + 2] as isize * matrix[4][4];
                        }
                    }
                }
                res += data[(x + 1) * w + y] as isize * matrix[3][2];
                if left {
                    if leftleft {
                        res += data[(x + 1) * w + (y - 2)] as isize * matrix[3][0];
                    }
                    res += data[(x + 1) * w + (y - 1)] as isize * matrix[3][1];
                    if right {
                        res += data[(x + 1) * w + y + 1] as isize * matrix[3][3];
                        if rightright {
                            res += data[(x + 1) * w + y + 2] as isize * matrix[3][4];
                        }
                    }
                }
            }

            res
        })
        .collect()
}

fn par_apply_3x3(data: &[u8], width: usize, height: usize, matrix: &[[isize; 3]; 3]) -> Vec<isize> {
    (0..(width * height)).into_par_iter()
        .map(|idx| {
            let mut top = data[idx] as isize * matrix[1][1];
            let x = idx / width;
            let y = idx % width;

            // top row of the matrix
            if x > 1 {
                if y > 1 {
                    top += data[(x - 1) * width + (y - 1)] as isize * matrix[0][0];
                }
                top += data[(x - 1) * width + y] as isize * matrix[0][1];
                if y < (width - 1) {
                    top += data[(x - 1) * width + y + 1] as isize * matrix[0][2];
                }
            }

            // middle row
            if y > 1 {
                top += data[x * width + (y - 1)] as isize * matrix[1][0];
            }
            if y < (width - 1) {
                top += data[x * width + y + 1] as isize * matrix[1][2];
            }

            // bottom row
            if x < (height - 1) {
                if y > 1 {
                    top += data[(x + 1) * width + (y - 1)] as isize * matrix[2][0];
                }
                top += data[(x + 1) * width + y] as isize * matrix[2][1];
                if y < (width - 1) {
                    top += data[(x + 1) * width + y + 1] as isize * matrix[2][2];
                }
            }

            top
        })
        .collect()
}

fn apply_3x3(data: &[u8], width: usize, height: usize, matrix: &[[isize; 3]; 3]) -> Vec<isize> {
    // Mouais
    assert!(width > 2);
    assert!(height > 2);

    let mut res = vec![];

    for idx in 0..(width * height) {
        let mut top = data[idx] as isize * matrix[1][1];
        let x = idx / width;
        let y = idx % width;

        // top row of the matrix
        if x > 1 {
            if y > 1 {
                top += data[(x - 1) * width + (y - 1)] as isize * matrix[0][0];
            }
            top += data[(x - 1) * width + y] as isize * matrix[0][1];
            if y < (width - 1) {
                top += data[(x - 1) * width + y + 1] as isize * matrix[0][2];
            }
        }

        // middle row
        if y > 1 {
            top += data[x * width + (y - 1)] as isize * matrix[1][0];
        }
        if y < (width - 1) {
            top += data[x * width + y + 1] as isize * matrix[1][2];
        }

        // bottom row
        if x < (height - 1) {
            if y > 1 {
                top += data[(x + 1) * width + (y - 1)] as isize * matrix[2][0];
            }
            top += data[(x + 1) * width + y] as isize * matrix[2][1];
            if y < (width - 1) {
                top += data[(x + 1) * width + y + 1] as isize * matrix[2][2];
            }
        }

        res.push(top);
    }

    res
}

fn par_magnitude(t1: &[isize], t2: &[isize]) -> Vec<u8> {
    t1.par_iter()
        .zip(t2.par_iter())
        .map(|(x, y)| {
            let powered = x * x + y * y;
            let mag = (powered as f32).sqrt().floor();
            mag as u8
        })
        .collect()
}

fn magnitude(t1: &[isize], t2: &[isize]) -> Vec<u8> {
    assert!(t1.len() == t2.len());

    let mut res = vec![];
    for (x, y) in t1.iter().zip(t2.iter()) {
        let powered = x * x + y * y;
        let mag = (powered as f32).sqrt().floor();
        res.push(mag as u8);
    }
    res
}

fn grey(input: &RgbImage) -> Vec<u8> {
    input.pixels()
        .map(|p| {
            let v = p[0] as u32 + p[1] as u32 + p[2] as u32;
            (v / 3) as u8
        })
        .collect()
}

fn sobel(input: &RgbImage) -> RgbImage {
    let (w, h) = input.dimensions();
    let mut res = RgbImage::new(w, h);

    let gx = [[-1, 0, 1],
              [-2, 0, 2],
              [-1, 0, 1]];
    let gy = [[-1, -2, -1],
              [0, 0, 0],
              [1, 2, 1]];

    let grey = grey(input);

    let (g_x, g_y) = rayon::join(|| apply_3x3(&grey, w as usize, h as usize, &gx),
                                 || apply_3x3(&grey, w as usize, h as usize, &gy));

    let mag = par_magnitude(&g_x, &g_y);

    res.pixels_mut().zip(mag.iter())
        .for_each(|(p, &v)| *p = image::Rgb([v, v, v]));

    res
}

fn sobel_5x5(input: &RgbImage) -> RgbImage {
    let (w, h) = input.dimensions();
    let mut res = RgbImage::new(w, h);

    let gx = [[0, 0, 0, 0, 0],
              [0, -1, 0, 1, 0],
              [0, -2, 0, 2, 0],
              [0, -1, 0, 1, 0],
              [0, 0, 0, 0, 0]];
    let gy = [[0, 0, 0, 0, 0],
              [0, -1, -2, -1, 0],
              [0, 0, 0, 0, 0],
              [0, 1, 2, 1, 0],
              [0, 0, 0, 0, 0]];

    let grey = grey(input);

    let (g_x, g_y) = rayon::join(|| apply_5x5(&grey, w as usize, h as usize, &gx),
                                 || apply_5x5(&grey, w as usize, h as usize, &gy));
    let mag = par_magnitude(&g_x, &g_y);

    res.pixels_mut().zip(mag.iter())
        .for_each(|(p, &v)| *p = image::Rgb([v, v, v]));

    res
}
fn laplacian_gaussian(input: &RgbImage) -> RgbImage {
    let (w, h) = input.dimensions();
    let mut res = RgbImage::new(w, h);

    let matrix = [[0, 0, -1, 0, 0],
                  [0, -1, -2, -1, 0],
                  [-1, -2, 16, -2, -1],
                  [0, -1, -2, -1, 0],
                  [0, 0, -1, 0, 0]];

    let grey = grey(input);

    let mag = apply_5x5(&grey, w as usize, h as usize, &matrix);
    let mag: Vec<u8> = mag.par_iter()
        .map(|v| ((v * v) as f32).sqrt().floor() as u8)
        .collect();

    res.pixels_mut().zip(mag.iter())
        .for_each(|(p, &v)| *p = image::Rgb([v, v, v]));

    res
}


use std::env;
use std::fs;
use std::path::Path;

fn main() {
    let mut files = vec![];

    let f = env::args().nth(1).unwrap();
    let m = fs::metadata(&f).unwrap();

    if m.is_file() {
        files.push(f);
    } else {
        for file in fs::read_dir(f).unwrap() {
            let ent = file.unwrap();
            let path = ent.path();

            if path.is_file() {
                files.push(path.to_str().unwrap().to_string())
            }
        }
    }

    files.par_iter()
        .for_each(|f| {
            let res = "sobel_".to_owned() + Path::new(&f).file_stem().unwrap().to_str().unwrap() + ".png";
            let img = image::open(f).unwrap().into_rgb8();
            sobel(&img).save(res).unwrap();
        })
}
